# Install docker-compose
sudo su
apt-get update
apt-get install -y\
    ca-certificates \
    curl \
    gnupg \
    lsb-release
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y  
# Install docker-cleanup command
cd /tmp
git clone https://gist.github.com/76b450a0c986e576e98b.git
cd 76b450a0c986e576e98b
mv docker-cleanup /usr/local/bin/docker-cleanup
chmod +x /usr/local/bin/docker-cleanup
# Install Tuba/
mkdir ~/Tuba/
cd ~/Tuba/ 
git clone git@gitlab.com:devisionx/ai-tool/tuba-frontend.git
cd tuba-frontend
docker build -t tuba .
docker run -it --rm -d -p 3001:3000 --name tuba tuba
